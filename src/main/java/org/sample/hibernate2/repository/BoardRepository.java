package org.sample.hibernate2.repository;

import java.util.List;

import org.sample.hibernate2.model.Board;

public interface BoardRepository {
	
	public List<Board> boardList();

}
