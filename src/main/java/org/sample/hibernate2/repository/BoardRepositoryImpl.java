package org.sample.hibernate2.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.sample.hibernate2.model.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class BoardRepositoryImpl implements BoardRepository{

	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public List<Board> boardList() {
		return sessionFactory.getCurrentSession().createCriteria(Board.class).addOrder(Property.forName("seq").desc()).list();
	}

}
