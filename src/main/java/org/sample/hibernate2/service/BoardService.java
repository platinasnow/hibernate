package org.sample.hibernate2.service;

import java.util.List;

import org.sample.hibernate2.model.Board;
import org.sample.hibernate2.repository.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoardService {

	@Autowired
	private BoardRepository boardRepository;
	
	public List<Board> boardList(){
		return boardRepository.boardList();
	}
}
