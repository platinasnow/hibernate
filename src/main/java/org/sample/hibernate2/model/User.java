package org.sample.hibernate2.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity(name = "usertable")
public class User {

	@Id
	private Integer pn;
	@Column(name = "userId")
	private String userId;
	@Column(name = "name")
	private String name;
	@Column(name = "password")
	private String password;
	@Column(name = "phoneNum")
	private String phoneNum;
	@Transient
	private List<String> authorities;

	@OneToMany(mappedBy = "user")
	private List<Board> board;

	public Integer getPn() {
		return pn;
	}

	public void setPn(Integer pn) {
		this.pn = pn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	public List<Board> getBoard() {
		return board;
	}

	public void setBoard(List<Board> board) {
		this.board = board;
	}

	@Override
	public String toString() {
		return "User [pn=" + pn + ", userId=" + userId + ", name=" + name + ", password=" + password + ", phoneNum=" + phoneNum + ", authorities="
				+ authorities + "]";
	}

}
