package org.sample.hibernate2.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity(name = "board")
public class Board {

	@Id
	@GeneratedValue
	private Integer seq = 0;
	@Column(name = "title")
	private String title;
	@Column(name = "contents")
	private String contents;
	@Column(name = "recodeDate")
	private Date recodeDate;
	@Column(name = "hit")
	private Integer hit;
	@Column(name = "author")
	private Integer author;
	@Column(name = "boardNum")
	private Integer boardNum = 1;

	@ManyToOne
	@JoinColumn(name = "author", referencedColumnName = "pn", insertable = false, updatable = false)
	private User user;

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Date getRecodeDate() {
		return recodeDate;
	}

	public void setRecodeDate(Date recodeDate) {
		this.recodeDate = recodeDate;
	}

	public Integer getHit() {
		return hit;
	}

	public void setHit(Integer hit) {
		this.hit = hit;
	}

	public Integer getAuthor() {
		return author;
	}

	public void setAuthor(Integer author) {
		this.author = author;
	}

	public Integer getBoardNum() {
		return boardNum;
	}

	public void setBoardNum(Integer boardNum) {
		this.boardNum = boardNum;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Board [seq=" + seq + ", title=" + title + ", contents=" + contents + ", recodeDate=" + recodeDate + ", hit=" + hit + ", author="
				+ author + ", boardNum=" + boardNum + ", user=" + user + "]";
	}

}
